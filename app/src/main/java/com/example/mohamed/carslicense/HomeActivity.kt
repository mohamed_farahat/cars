package com.example.mohamed.carslicense

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth

class HomeActivity : AppCompatActivity() {

    //Firebase
    private var mAuth: FirebaseAuth? = null

    private lateinit var mSignOut: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        mAuth = FirebaseAuth.getInstance()

        mSignOut = findViewById(R.id.Logout)
        mSignOut.setOnClickListener { view ->
            signOut()
        }
    }

    private fun signOut() {
        mAuth!!.signOut()
        val intent = Intent(this, SignInActivity::class.java)
        startActivity(intent)
    }
}
