package com.example.mohamed.carslicense

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.util.Patterns
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import android.support.annotation.NonNull
import android.content.ClipData.newIntent


class SignInActivity : AppCompatActivity() {

    private val TAG = SignInActivity::class.java!!.simpleName
    private lateinit var signInUserEmailEditText: EditText
    private lateinit var signInUserPasswordEditText: EditText
    private lateinit var loginBotton: TextView
    private lateinit var signUpButton: TextView

    //Firebase
    private var mAuth: FirebaseAuth? = null
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null


    override fun onStart() {
        super.onStart()
        mAuthListener?.let { mAuth?.addAuthStateListener(it) }
    }


    public override fun onStop() {
        super.onStop()
        if (mAuthListener != null) {
            mAuth!!.removeAuthStateListener(mAuthListener!!)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        initialScreen()

        mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser
            if (user != null) {
                // User is signed in
                // Toast.makeText(LoginActivity.this, "onAuthStateChanged:signed_in:", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onAuthStateChanged:signed_in:" + user.uid)
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)

            } else {
                // User is signed out
                // Toast.makeText(LoginActivity.this, "onAuthStateChanged:signed_out:", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onAuthStateChanged:signed_out")
            }
            // ...
        }
        mAuth!!.addAuthStateListener(mAuthListener!!)

        val mAuthprogressDialog = ProgressDialog(this)
        mAuthprogressDialog.setMessage(getResources().getString(R.string.progress_dialog_login_with_firebase))
        mAuthprogressDialog.setTitle(getResources().getString(R.string.progress_dialog_loading))
        mAuthprogressDialog.setCancelable(false)

        signUpButton.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        loginBotton.setOnClickListener {
            val userEmail: String = signInUserEmailEditText.text.toString()
            val userPassword: String = signInUserPasswordEditText.text.toString()

            var validEmail = isValidEmail(userEmail)
            var validPassword = isValidPassword(userPassword)

            if (!validPassword || !validEmail) return@setOnClickListener

            mAuthprogressDialog.show()
            mAuth!!.signInWithEmailAndPassword(userEmail!!, userPassword!!)
                    .addOnCompleteListener(this) { task ->
                        if (!task.isSuccessful) {
                            // If sign in fails, display a message to the user.

                            Log.d(TAG, "signInWithEmail:failed")
                            mAuthprogressDialog.dismiss()

                            try {
                                throw task.exception!!
                            } catch (e: FirebaseAuthInvalidCredentialsException) {
                                signInUserPasswordEditText.setError(getString(R.string.error_invalid_password_not_correct))
                                signInUserPasswordEditText.requestFocus()
                            } catch (e: FirebaseAuthInvalidUserException) {
                                signInUserEmailEditText.setError(getString(R.string.error_message_email_issue))
                                signInUserEmailEditText.requestFocus()
                            } catch (e: FirebaseNetworkException) {
                                showErrorToast(getString(R.string.error_message_failed_sign_in_no_network))
                            } catch (e: FirebaseTooManyRequestsException) {
                                Toast.makeText(baseContext, R.string.many_login_requests, Toast.LENGTH_SHORT).show()
                            } catch (e: Exception) {
                                Log.e(TAG, e.toString() + "")
                            }

                        } else {
                            // Sign in success, update UI with signed-in user's information
                            Log.d(TAG, "signInWithEmail:success")
                            val intent = Intent(this, HomeActivity::class.java)
                            startActivity(intent)
                            mAuthprogressDialog.dismiss()

                        }
                    }
        }

    }

    private fun isValidEmail(email: String): Boolean {
        return if (email.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            true
        } else {
            signInUserEmailEditText.error = getString(R.string.error_invalid_email_not_valid)
            false
        }
    }

    private fun isValidPassword(password: String): Boolean {
        return if (password.isNotEmpty() && password.length >= 6) {
            true
        } else {
            signInUserPasswordEditText.error = getString(R.string.error_invalid_password_not_valid)
            false
        }
    }

    private fun showErrorToast(message: String) {
        Toast.makeText(this@SignInActivity, message, Toast.LENGTH_LONG).show()
    }

    private fun initialScreen() {
        signInUserEmailEditText = findViewById(R.id.signin_user_email)
        signInUserPasswordEditText = findViewById(R.id.signin_user_password)
        loginBotton = findViewById(R.id.signIn_login_Button)
        signUpButton = findViewById(R.id.signUp_login_Button)

        mAuth = FirebaseAuth.getInstance()

    }
}
