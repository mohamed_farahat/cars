package com.example.mohamed.carslicense

data class User(val email: String, val userName: String, val phone: String, val haveCar: Boolean, val carNumber: String? = null, val plateColor: String? = null)