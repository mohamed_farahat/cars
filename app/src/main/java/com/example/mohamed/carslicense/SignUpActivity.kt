package com.example.mohamed.carslicense

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import org.angmarch.views.NiceSpinner

class SignUpActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var signInButton: TextView
    private lateinit var signUpEmailEditText: EditText
    private lateinit var signUpUserNameEditText: EditText
    private lateinit var signUpPasswordEditText: EditText
    private lateinit var signUpPhoneEditText: EditText
    private lateinit var signUpButton: TextView
    private lateinit var haveCarCheckBox: CheckBox

    lateinit var carPlateColorSpinner: NiceSpinner
    private lateinit var dialogCarNumberChar: EditText
    private lateinit var dialogCarNumberNum: EditText
    private val TAG = SignUpActivity::class.java!!.simpleName
    //Firebase
    private var mAuth: FirebaseAuth? = null
    private var mDatabase: DatabaseReference? = null
    private var mUsersReference: DatabaseReference? = null

    var carPlateNumber: String = ""
    var carPlateColor: String = ""
    lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        val mAuthprogressDialog = ProgressDialog(this)
        mAuthprogressDialog.setMessage(getResources().getString(R.string.progress_dialog_creating_user_with_firebase))
        mAuthprogressDialog.setTitle(getResources().getString(R.string.progress_dialog_loading))
        mAuthprogressDialog.setCancelable(false)
        initialScreen()



        signInButton.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
        }

        signUpButton.setOnClickListener {

            val userEmail: String = signUpEmailEditText.text.toString()
            val userPassword: String = signUpPasswordEditText.text.toString()
            val userUserName: String = signUpUserNameEditText.text.toString()
            val userPhone: String = signUpPhoneEditText.text.toString()
            val checkedCar: Boolean = haveCarCheckBox.isChecked

            var validEmail = isValidEmail(userEmail)
            var validPassword = isValidPassword(userPassword)
            var validUserName = isValidUserName(userUserName)
            var validPhoneNumber = isValidPhoneNumber(userPhone)

            if (!validPassword || !validEmail || !validUserName || !validPhoneNumber) return@setOnClickListener

            mAuthprogressDialog.show()
            mAuth!!.createUserWithEmailAndPassword(userEmail, userPassword)
                    .addOnCompleteListener(this) { task ->
                        if (!task.isSuccessful) {
                            // Sign in: fail
                            mAuthprogressDialog.dismiss()
                            try {
                                throw task.exception!!
                            } catch (e: FirebaseAuthWeakPasswordException) {

                                signUpPasswordEditText.error = getString(R.string.error_invalid_password_not_valid)
                                signUpPasswordEditText.requestFocus()

                            } catch (e: FirebaseAuthInvalidCredentialsException) {

                                signUpEmailEditText.error = String.format(getString(R.string.error_invalid_email_not_valid),
                                        userEmail)
                                signUpEmailEditText.requestFocus()

                            } catch (e: FirebaseAuthUserCollisionException) {

                                signUpEmailEditText.error = getString(R.string.error_email_taken)
                                signUpEmailEditText.requestFocus()

                            } catch (e: FirebaseNetworkException) {

                                showErrorToast(getString(R.string.network_error))

                            } catch (e: Exception) {
                                Log.e(TAG, e.message)
                            }
                        } else {
                            // Sign in: success
                            // update UI for current User
                            val currentUserUid = mAuth!!.currentUser!!.uid

                            //create user in realtime database
                            mDatabase = FirebaseDatabase.getInstance().reference
                            mUsersReference = FirebaseDatabase.getInstance().getReference("Users")

                            user = if (checkedCar)
                                User(userEmail, userEmail, userPhone, checkedCar, carPlateNumber, carPlateColor)
                                  else
                                User(userEmail, userEmail, userPhone, checkedCar)

                            mUsersReference!!.child(currentUserUid).setValue(user)

                            val intent = Intent(this, HomeActivity::class.java)
                            startActivity(intent)

                            mAuthprogressDialog.dismiss()

                        }
                    }
        }

    }

    private fun isValidEmail(email: String): Boolean {
        return if (email.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            true
        } else {
            signUpEmailEditText.error = getString(R.string.error_invalid_email_not_valid)
            false
        }
    }

    private fun isValidPassword(password: String): Boolean {
        return if (password.isNotEmpty() && password.length >= 6) {
            true
        } else {
            signUpPasswordEditText.error = getString(R.string.error_invalid_password_not_valid)
            false
        }
    }

    private fun isValidPhoneNumber(phone: String): Boolean {
        return if (phone.isNotEmpty() && phone.length == 11) {
            true
        } else {
            signUpPhoneEditText.error = getString(R.string.error_invalid_phone_not_valid)
            false
        }
    }

    private fun isValidUserName(userName: String): Boolean {
        return if (userName.isNotEmpty() && userName.length >= 4) {
            true
        } else {
            signUpUserNameEditText.error = getString(R.string.error_invalid_username_not_valid)
            false
        }
    }

    private fun showErrorToast(message: String) {
        Toast.makeText(this@SignUpActivity, message, Toast.LENGTH_LONG).show()
    }

    private fun initialScreen() {
        signUpEmailEditText = findViewById(R.id.signUp_email_editText)
        signUpUserNameEditText = findViewById(R.id.signUp_userName_editText)
        signUpPasswordEditText = findViewById(R.id.signUp_password_editText)
        signUpPhoneEditText = findViewById(R.id.signUp_phone_editText)
        signInButton = findViewById(R.id.signUp_login_textView)
        signUpButton = findViewById(R.id.signUp_registrate_textview)
        haveCarCheckBox = findViewById(R.id.have_car_checkBox)
        mAuth = FirebaseAuth.getInstance()



        haveCarCheckBox?.setOnCheckedChangeListener { buttonView, isChecked ->
            //            val msg = "You have " + (if (isChecked) "checked" else "unchecked") + " this Check it Checkbox."
            if (isChecked) {
                val dialogView = LayoutInflater.from(this).inflate(R.layout.activity_car_data, null)

                var title = "Car Data"
                var carPlateColors = arrayListOf("Light blue", "Orange", "Blue", "Red", "Brown", "Yellow", "Green", "Light blue", "Orange")

                val builder = AlertDialog.Builder(this)
                        .setView(dialogView)
                        .setTitle(title)
                        .setPositiveButton("Submit", null)
                        .setNegativeButton("Cancel", null)

                //car data
                carPlateColorSpinner = dialogView.findViewById(R.id.plate_spinner)
                dialogCarNumberChar = dialogView.findViewById(R.id.car_char_edit_text)
                dialogCarNumberNum = dialogView.findViewById(R.id.car_number_edit_text)


                carPlateColorSpinner.attachDataSource(carPlateColors)
                carPlateColorSpinner.setTextColor(Color.parseColor("#87ceeb"))

                carPlateColorSpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                        Toast.makeText(applicationContext, carPlateColors[position], Toast.LENGTH_SHORT).show()
                        if (carPlateColors[position] == "Light blue")
                            carPlateColorSpinner.setTextColor(Color.parseColor("#87ceeb"))
                        if (carPlateColors[position] == "Orange")
                            carPlateColorSpinner.setTextColor(Color.parseColor("#FF4500"))
                        if (carPlateColors[position] == "Blue")
                            carPlateColorSpinner.setTextColor(Color.parseColor("#0000FF"))
                        if (carPlateColors[position] == "Red")
                            carPlateColorSpinner.setTextColor(Color.parseColor("#FF0000"))
                        if (carPlateColors[position] == "Brown")
                            carPlateColorSpinner.setTextColor(Color.parseColor("#8B4513"))
                        if (carPlateColors[position] == "Yellow")
                            carPlateColorSpinner.setTextColor(Color.parseColor("#FFFF00"))
                        if (carPlateColors[position] == "Green")
                            carPlateColorSpinner.setTextColor(Color.parseColor("#228B22"))

                        carPlateColor = carPlateColors[position]
                    }
                })

                val dialog = builder.show()

                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener {
                    dialog.dismiss()
                    haveCarCheckBox.isChecked = false
                }
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                    carPlateNumber = dialogCarNumberChar.text.toString() + dialogCarNumberNum.text.toString()

                    dialog.dismiss()
                    Toast.makeText(applicationContext, "Login is submitted, verify the credential and log the user into the app", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }
}
